package com.s4n.validation;

import com.s4n.dto.Direction;
import org.junit.Test;

public class DTOValidatorTest {

    @Test(expected = Exception.class)
    public void validateXPositiveOutRangeTest() throws Exception {

        final Direction direction = new Direction();
        direction.setX(12);
        DTOValidator.validate(direction);
    }

    @Test(expected = Exception.class)
    public void validateXNegativeOutRangeTest() throws Exception {

        final Direction direction = new Direction();
        direction.setX(-12);
        DTOValidator.validate(direction);
    }

    @Test(expected = Exception.class)
    public void validateYPositiveOutRangeTest() throws Exception {

        final Direction direction = new Direction();
        direction.setY(12);
        DTOValidator.validate(direction);
    }

    @Test(expected = Exception.class)
    public void validateYNegativeOutRangeTest() throws Exception {

        final Direction direction = new Direction();
        direction.setY(-12);
        DTOValidator.validate(direction);
    }

    @Test
    public void testValidateFileSize() throws Exception {

    }
}