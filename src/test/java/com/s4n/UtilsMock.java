package com.s4n;

import java.io.File;

/**
 * Created by Entelgy on 12/08/2016.
 */
public class UtilsMock {

    public static String getURL(final String path) {
        final File file = new File(path);
        String absolutePath = file.getAbsolutePath();
        absolutePath = absolutePath.replace("..", "src\\main");
        return absolutePath;
    }
}
