package com.s4n.file;

import com.s4n.UtilsMock;
import com.s4n.dto.Coordinates;
import com.s4n.dto.Direction;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

public class EvaluatorOrderTest {

    @InjectMocks
    private EvaluatorOrder evaluatorOrder;

    @Mock
    private ReadOrdersFile readOrdersFile = Mockito.mock(ReadOrdersFile.class);

    private static final String ROUTE_OK = "../resources/in.txt";

    @Before
    public void init() {
        evaluatorOrder = new EvaluatorOrder();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetCoordinates() throws Exception {

        final List<String> list = new ArrayList<String>();
        list.add("DDAIAD");
        list.add("AAIADAD");

        Mockito.when(readOrdersFile.getFileCoordinates(Mockito.anyString())).thenReturn(list);

        final List<Coordinates> coordinates = evaluatorOrder.getCoordinates(UtilsMock.getURL(ROUTE_OK));
        Assert.assertNotNull(coordinates);
    }

    @Test
    public void getCoordinateReferenceTest() throws Exception {

        final Coordinates coordinateReference = evaluatorOrder.getCoordinateReference("   ");

        Assert.assertNotNull(coordinateReference);
    }

    @Test
    public void updateCoordinatesRightWithAllOrientationOneTest() throws Exception {

        final Character character = 'D';
        final Integer advance = 1;

        final Direction result = evaluatorOrder.updateCoordinates(new Direction(), character, advance);

        Assert.assertNotNull(result);
    }

    @Test
    public void updateCoordinatesRightWithAllOrientationTwoTest() throws Exception {

        final Character character = 'D';
        final Integer advance = 1;

        final Direction direction = new Direction();
        direction.setOrientation(2);

        final Direction result = evaluatorOrder.updateCoordinates(direction, character, advance);

        Assert.assertNotNull(result);
    }

    @Test
    public void updateCoordinatesRightWithAllOrientationThreeTest() throws Exception {

        final Character character = 'D';
        final Integer advance = 1;

        final Direction direction = new Direction();
        direction.setOrientation(3);

        final Direction result = evaluatorOrder.updateCoordinates(direction, character, advance);

        Assert.assertNotNull(result);
    }

    @Test
    public void updateCoordinatesRightWithAllOrientationFourTest() throws Exception {

        final Character character = 'D';
        final Integer advance = 1;

        final Direction direction = new Direction();
        direction.setOrientation(4);

        final Direction result = evaluatorOrder.updateCoordinates(direction, character, advance);

        Assert.assertNotNull(result);
    }


    @Test
    public void updateCoordinatesLeftWithAllOrientationOneTest() throws Exception {

        final Character character = 'I';
        final Integer advance = 1;

        final Direction result = evaluatorOrder.updateCoordinates(new Direction(), character, advance);

        Assert.assertNotNull(result);
    }

    @Test
    public void updateCoordinatesLeftWithAllOrientationTwoTest() throws Exception {

        final Character character = 'I';
        final Integer advance = 1;

        final Direction direction = new Direction();
        direction.setOrientation(2);

        final Direction result = evaluatorOrder.updateCoordinates(direction, character, advance);

        Assert.assertNotNull(result);
    }

    @Test
    public void updateCoordinatesLeftWithAllOrientationThreeTest() throws Exception {

        final Character character = 'I';
        final Integer advance = 1;

        final Direction direction = new Direction();
        direction.setOrientation(3);

        final Direction result = evaluatorOrder.updateCoordinates(direction, character, advance);

        Assert.assertNotNull(result);
    }

    @Test
    public void updateCoordinatesLeftWithAllOrientationFourTest() throws Exception {

        final Character character = 'I';
        final Integer advance = 1;

        final Direction direction = new Direction();
        direction.setOrientation(4);

        final Direction result = evaluatorOrder.updateCoordinates(direction, character, advance);

        Assert.assertNotNull(result);
    }


}