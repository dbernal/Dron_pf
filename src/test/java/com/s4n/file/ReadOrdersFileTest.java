package com.s4n.file;

import com.s4n.UtilsMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

public class ReadOrdersFileTest {

    @InjectMocks
    private ReadOrdersFile readOrdersFile;

    private static final String ROUTE_OK = "../resources/in.txt";
    private static final String ROUTE_ERROR = "../resources/archivoMaxLength.txt";

    @Before
    public void setUp() {
        readOrdersFile = new ReadOrdersFile();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetFileCoordinates() throws Exception {
        final List<String> fileCoordinates = readOrdersFile.getFileCoordinates(UtilsMock.getURL(ROUTE_OK));
        Assert.assertNotNull(fileCoordinates);
    }

    @Test(expected = Exception.class)
    public void testGetFileCoordinatesMaxLengthFileErrorTest() throws Exception {
        readOrdersFile.getFileCoordinates(UtilsMock.getURL(ROUTE_ERROR));
    }

    @Test(expected = Exception.class)
    public void getFileCoordinatesNotFountTest() throws Exception {

        Mockito.when((readOrdersFile).getFileCoordinates("")).thenThrow(Exception.class);

        readOrdersFile.getFileCoordinates(UtilsMock.getURL(ROUTE_OK));

    }

    @Test(expected = Exception.class)
    public void getFileCoordinatesRouteNullTest() throws Exception {

        Mockito.when((readOrdersFile).getFileCoordinates(null)).thenThrow(Exception.class);

        readOrdersFile.getFileCoordinates(UtilsMock.getURL(ROUTE_OK));
    }
}