package com.s4n.file;

import com.s4n.utils.Constant;
import com.s4n.utils.NormalizeText;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Entelgy on 08/08/2016.
 */
public class ReadOrdersFile extends NormalizeText {

    protected List<String> getFileCoordinates(final String route) throws Exception {

        final List<String> coordinatesList = new ArrayList<String>();

        FileReader fileReader = null;

        try {
            final File file = new File(route);
            fileReader = new FileReader(file);
            final BufferedReader bufferedReader = new BufferedReader(fileReader);

            String line;
            Integer counter = 1;

            while ((line = bufferedReader.readLine()) != null) {

                if (counter > Constant.MAXIMUM_FILE_SIZE)
                    throw new Exception(Constant.FILE_SIZE_EXCEPTION);

                coordinatesList.add(normalizeCoordinates(line.trim()));
                counter++;
            }
        } catch (FileNotFoundException e) {
            throw new Exception(Constant.FILE_NOT_FOUND_EXCEPTION);
        } catch (final Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            fileReader.close();
        }
        return coordinatesList;
    }
}
