package com.s4n.file;

import com.s4n.dto.Coordinates;
import com.s4n.utils.Constant;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Entelgy on 08/08/2016.
 */
public class WriteDeliveries {

    public void writer(final List<Coordinates> coordinates, final String route) throws Exception {

        FileWriter file = null;
        PrintWriter printWriter = null;
        try {
            file = new FileWriter(route.replace("in", "out"));
            printWriter = new PrintWriter(file);

            for (final Coordinates coordinate : coordinates) {

                final Iterator iterator = coordinate.getOrientation().keySet().iterator();

                while (iterator.hasNext()) {
                    final String key = (String) iterator.next();

                    final String out = "(" +
                            coordinate.getOrientation().get(key).getX() + ", " +
                            coordinate.getOrientation().get(key).getY() + ")" + "" +
                            coordinate.getOrientation().get(key).getDirection().getValue();

                    printWriter.println(out);
                }
            }
        } catch (FileNotFoundException e) {
            throw new Exception(Constant.FILE_NOT_FOUND_EXCEPTION);
        } catch (final Exception e) {
            throw new Exception(e.getMessage());
        }finally {
            printWriter.close();
        }
    }
}

