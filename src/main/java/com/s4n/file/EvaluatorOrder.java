package com.s4n.file;

import com.s4n.dto.Coordinates;
import com.s4n.dto.Direction;
import com.s4n.utils.Constant;
import com.s4n.utils.EnumDirection;
import com.s4n.validation.DTOValidator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Entelgy on 08/08/2016.
 */
public class EvaluatorOrder {

    public List<Coordinates> getCoordinates(final String route) throws Exception {

        final List<Coordinates> coordinatesList = new ArrayList<Coordinates>();

        final List<String> fileCoordinates = new ReadOrdersFile().getFileCoordinates(route);

        Coordinates coordinateReference = null;

        for (final String fileCoordinate : fileCoordinates) {
            coordinateReference = getCoordinateReference(fileCoordinate);

            coordinatesList.add(coordinateReference);
        }

        return coordinatesList;
    }

    public Coordinates getCoordinateReference(final String coordinate) throws Exception {

        final Coordinates coordinates = new Coordinates();
        Integer count = 0;
        Integer stopPoint = 0;
        Character planeDirection = null;

        while (stopPoint <= coordinate.length() - 1) {

            planeDirection = coordinate.charAt(stopPoint);

            for (int j = stopPoint; j < coordinate.length() - 1; j++) {

                final Integer length = (j + 1) < coordinate.length() ? (j + 1) : j;

                if (Constant.AHEAD.equals(coordinate.charAt(length))) {
                    count++;
                    stopPoint++;
                } else {
                    break;
                }
            }

            final Direction direction = coordinates.getOrientation().get(coordinate);
            final Direction updateObject = updateCoordinates(direction, planeDirection, count);
            coordinates.getOrientation().put(coordinate, updateObject);

            count = 0;
            stopPoint++;
        }

        return coordinates;
    }

    public Direction updateCoordinates(Direction direction, final Character turn, final Integer advance) throws Exception {

        if (direction == null)
            direction = new Direction();

        if (direction.getOrientation() == 1 && Constant.RIGHT.equals(turn)) {
            direction.setOrientation(2);
            direction.setDirection(EnumDirection.EAST);
            direction.setX(advance);
        } else if (direction.getOrientation() == 2 && Constant.RIGHT.equals(turn)) {
            direction.setOrientation(3);
            direction.setDirection(EnumDirection.SOUTH);
            direction.setY(-advance);
        } else if (direction.getOrientation() == 3 && Constant.RIGHT.equals(turn)) {
            direction.setOrientation(4);
            direction.setDirection(EnumDirection.WEST);
            direction.setX(-advance);
        } else if (direction.getOrientation() == 4 && Constant.RIGHT.equals(turn)) {
            direction.setOrientation(1);
            direction.setDirection(EnumDirection.NORTH);
            direction.setY(advance);
        } else if (direction.getOrientation() == 1 && Constant.LEFT.equals(turn)) {
            direction.setOrientation(4);
            direction.setDirection(EnumDirection.WEST);
            direction.setX(-advance);
        } else if (direction.getOrientation() == 2 && Constant.LEFT.equals(turn)) {
            direction.setOrientation(1);
            direction.setDirection(EnumDirection.NORTH);
            direction.setY(advance);
        } else if (direction.getOrientation() == 3 && Constant.LEFT.equals(turn)) {
            direction.setOrientation(2);
            direction.setDirection(EnumDirection.EAST);
            direction.setX(advance);
        } else if (direction.getOrientation() == 4 && Constant.LEFT.equals(turn)) {
            direction.setOrientation(3);
            direction.setDirection(EnumDirection.SOUTH);
            direction.setY(-advance);
        }else {
            direction.setY(advance);
        }

        DTOValidator.validate(direction);

        return direction;
    }
}
