package com.s4n.validation;

import com.s4n.dto.Direction;
import com.s4n.utils.Constant;

import java.io.BufferedReader;

/**
 * Created by Entelgy on 11/08/2016.
 */
public class DTOValidator {

    public static void validate(final Direction direction) throws Exception {
        if ((direction.getX() < -10 || direction.getX() > 10) || (direction.getY() < -10 || direction.getY() > 10))
            throw new Exception(Constant.MAXIMUM_DISTANCE_EXCEPTION);
    }
}
