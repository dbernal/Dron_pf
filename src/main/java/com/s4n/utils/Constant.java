package com.s4n.utils;

/**
 * Created by Entelgy on 08/08/2016.
 */
public class Constant {

    //Orientation
    public static final Character AHEAD = 'A';
    public static final Character LEFT = 'I';
    public static final Character RIGHT = 'D';

    //File
    public static final Integer MAXIMUM_FILE_SIZE = 10;

    //Errors
    public static final String FILE_SIZE_EXCEPTION = "Maximo cargar hasta tres almuerzos por vez";
    public static final String MAXIMUM_DISTANCE_EXCEPTION = "Sólo entregan domicilios a 10 cuadras a la redonda";
    public static final String FILE_NOT_FOUND_EXCEPTION = "Archivo especificado no existe";
    public static final String CHARACTER_INVALID_EXCEPTION = "Archivo con caracteres no validos";
    public static final String EMPTY_PARAMETERS = "Especifique la ruta del archivo in.txt";
}
