package com.s4n.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Entelgy on 11/08/2016.
 */
public class NormalizeText {

    protected String normalizeCoordinates(String text) throws Exception {

        isInvalidText(text);
        if (text.charAt(0) == 'A')
            text = "N" + text;

        return text;
    }

    protected boolean isInvalidText(final String text) throws Exception {

        Pattern pat = Pattern.compile("(A|D|I|N)+");
        Matcher mat = pat.matcher(text);

        if (!mat.matches()) {
            throw new Exception(Constant.CHARACTER_INVALID_EXCEPTION);
        }
        return true;
    }
}
