package com.s4n.main;

import com.s4n.prueba.Pitcher;
import com.s4n.utils.Constant;

import java.io.File;

/**
 * Created by Entelgy on 08/08/2016.
 */
public class Main {

    public static void main(String[] args) {

        if (args.length == 0) {
            System.out.println(Constant.EMPTY_PARAMETERS);

        } else {
            final String route = args[0];

            final File file = new File(route);
            final File listFiles[] = file.listFiles();

            for (final File listFile : listFiles) {
                if (listFile.getName().contains("in")) {
                    final Pitcher pitcher = new Pitcher(listFile.getPath());
                    pitcher.run();
                }
            }
        }
    }
}
