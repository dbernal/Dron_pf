package com.s4n.prueba;

import com.s4n.dto.Coordinates;
import com.s4n.file.EvaluatorOrder;
import com.s4n.file.WriteDeliveries;

import java.util.List;

/**
 * Created by Entelgy on 12/08/2016.
 */
public class Pitcher extends Thread {

    private String route;

    public Pitcher(String route) {
        this.route = route;
    }

    @Override
    public void run() {
        synchronized (EvaluatorOrder.class) {
            try {
                final List<Coordinates> coordinates = new EvaluatorOrder().getCoordinates(route);

                new WriteDeliveries().writer(coordinates, route);
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
    }
}
